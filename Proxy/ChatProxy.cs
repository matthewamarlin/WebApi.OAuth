using System;
using System.Net;
using Microsoft.AspNet.SignalR.Client;
using SpearOne.Examples.OAuth.Service.Contracts;

namespace SpearOne.Examples.OAuth.Service.Proxy
{
    public class ChatProxy : IDisposable, IChatHubClient
    {
        private readonly IHubProxy _hubProxy;
        private readonly HubConnection _hubConnection;

        public event EventHandler<string> ReceivedMessageEvent;

        public ChatProxy(string connectionString = null)
        {
            _hubConnection = new HubConnection(connectionString ?? "http://localhost:4205/signalr");
#if DEBUG            
            //_hubConnection.TraceLevel = TraceLevels.All;
            //_hubConnection.TraceWriter = Console.Out;
#endif
            _hubProxy = _hubConnection.CreateHubProxy("ChatHub");
            _hubProxy.On<string, string>("ReceiveMessage", ReceiveMessage);

            _hubConnection.Start().Wait();
            ServicePointManager.DefaultConnectionLimit = 10; //TODO Does this work?
        }

        #region Client Commands

        public void ReceiveMessage(string sender, string message)
        {
            ReceivedMessageEvent?.Invoke(this, message);
        }

        #endregion

        #region Server Commands

        public void Send(string sender, string message)
        {
            _hubProxy.Invoke("Send", sender, message).Wait();
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            _hubConnection?.Dispose();
        }

        #endregion
    }
}