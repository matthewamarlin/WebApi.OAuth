﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.OAuth.Service.Proxy")]
[assembly: AssemblyDescription("A class library containing a proxy implementation for interacting with the SpearOne.Examples.OAuth.Service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("8D16F7FD-3B09-4C19-A674-1313B78EAA36")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]