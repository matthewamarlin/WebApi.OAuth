MERGE INTO dbo.DimUnitOfMeasure AS Target 
USING (VALUES 
( 'Each' ,NULL,Null) 
,( 'Gram' ,NULL,null) 
,( 'Eighth' ,NULL,NULL) 
,( 'Quarter',NULL,NULL) 
,( 'Half',NULL,NULL) 
,( 'Ounce',NULL,NULL) 
,( 'Pound',NULL,NULL) 
,( 'Pint',NULL,NULL) 
,( 'Quart',NULL,NULL) 
,( 'Gallon',NULL,NULL) 
,( 'Gallon',NULL,NULL) 
,( 'Milliliter',NULL,NULL) 
,( 'Liter',NULL,NULL) 
)
AS Source ([UOMDescription], [ConversionFactor], [ConversionKey]) 
ON Target.[UOMDescription] = Source.[UOMDescription] 
-- update matched rows 
WHEN MATCHED THEN 
UPDATE SET [ConversionFactor] = Source.[ConversionFactor],  [ConversionKey] = Source.[ConversionKey]
-- insert new rows 
WHEN NOT MATCHED BY TARGET THEN 
INSERT ([UOMDescription], [ConversionFactor], [ConversionKey]) 
VALUES ([UOMDescription], [ConversionFactor], [ConversionKey]);
GO