
--Prepare Dimensions
DECLARE @ENDDATE DATETIME,
       @STARTDATE DATETIME,
	   @CURRENCYKEY INT,
	   @DATAVENDOR INT,
	   @ETLID INT

SET @STARTDATE = GETDATE()
SET @ENDDATE = '9999-01-01'
SET @ETLID =1

PRINT 'Deleting'
TRUNCATE TABLE dbo.FactSales
TRUNCATE TABLE dbo.FactProductInventory
DELETE FROM dbo.FactProductInventory
DELETE FROM dbo.DimProduct
DELETE FROM dbo.DimProductCategory
DELETE FROM dbo.DimEmployee
DELETE FROM dbo.DimOrganization
DELETE FROM dbo.DimCurrency
DELETE FROM dbo.DimDataVendor


--------------------------DimCUrrency---------------------------------------------------------
PRINT 'Inserting Currency'

INSERT INTO dbo.DimCurrency (CurrencyAlternateKey,CurrencyName)
VALUES('USD','US Dollar')

--------------------------DimDate---------------------------------------------------------
--Done with script DIMDatePopulate.sql

--------------------------DimDatavendor---------------------------------------------------------
PRINT 'Inserting Vendor'
INSERT INTO dbo.DimDataVendor (VendorName, VendorID, ETLID)
VALUES ('Patrick Excel Sample data','PAT',1)


--------------------------DimPRODUCT---------------------------------------------------------
PRINT 'Inserting Product'

/* --- moved the categories into the main dimension for now
INSERT INTO dbo.DimProductCategory
(
ProductCategoryAlternateKey
,ProductCategoryName
)
SELECT DISTINCT Item_Group_Category
,Item_Group_Category
FROM dbo.['Inventory$']


INSERT INTO dbo.DimProductSubCategory
(
ProductSubCategoryAlternateKey
,ProductSubcategoryName
,ProductcategoryKey
)
SELECT DISTINCT Item_Group_Description
,Item_Group_Description
,cat.ProductCategoryKey
FROM dbo.['Inventory$'] inv
JOIN dbo.DimProductCategory cat on cat.ProductCategoryAlternateKey = inv.Item_Group_Category
WHERE Item_Group_Description != 'null'

*/

INSERT INTO dbo.DimProduct (ProductAlternateKey
,ItemName
,ItemType
,ItemGroupDescription
,ItemGroupCategory
,Description
,UnitOfMeasure
,StartDate
,EndDate
,CurrentFlag
,ETLID)
SELECT DISTINCT Item_Code
,Item_Name
,Item_Type
,Item_Group_Description
,ITem_Group_Category
,Description
,UOM_Desc
,@STARTDATE
, @ENDDATE
,1
,@ETLID
 FROM dbo.['Inventory$'] 

-------------------------DimEmployee--------------------------------------------------------
PRINT 'Inserting Employee'

INSERT INTO dbo.DimEmployee 
(
ParentEmployeeKey
,EmployeeNationalIDAlternateKey
,ParentEmployeeNationalIDAlternateKey
,SalesTerritoryKey
,FirstName
,LastName
,MiddleName
,NameStyle
,Title
,HireDate
,BirthDate
,Phone
,MaritalStatus
,EmergencyContactName
,EmergencyContactPhone
,Gender
,SalesPersonFlag
,DepartmentName
,EmployeePhoto
,StartDate
,EndDate
,CurrentFlag
,ETLID)
SELECT 
null
,e.Personal_SSN
,null
,1
,e.Personal_FirstName
,e.Personal_MiddleName
,e.Personal_LastName
,0
,e.Personal_Prefix
,null
,null
,e.Business_Phone
,null
,null
,null
,null
,1
,null
,null
,@STARTDATE
,@ENDDATE
,1
,@ETLID
FROM dbo.['Employee List$'] e

-------------------------DIMOrganization--------------------------------------------------------
PRINT 'Inserting Organization'

SELECT @CURRENCYKEY = CurrencyKey FROM DimCurrency WHERE CurrencyAlternateKey = 'USD'

INSERT INTO dbo.DimOrganization (OrganizationAlternateKey
,OrganizationName
,OrganizationDescription
,OrganizationCategory
,OrganizationState
,ProjectedSales
,CurrencyKey
,StartDate
,EndDate
,CurrentFlag
,ETLID)
SELECT DISTINCT Company_ID
,Business_CompanyName
, Business_Description
,Business_Category 
,Business_State
,Projected_Annual_Sales
,@CURRENCYKEY
,@STARTDATE
,@ENDDATE
,1
,@ETLID
FROM dbo.['Client List$']
WHERE Company_ID IS NOT NULL


---------------------------------Fact Sales------------------------------------------------
PRINT 'Inserting Sales'

SELECT @CURRENCYKEY = CurrencyKey FROM DimCurrency WHERE CurrencyAlternateKey = 'USD'
SELECT @DATAVENDOR = VendorKey FROM DimDataVendor 

--Prepare FACTs
INSERT INTO dbo.FactSales 
(
ProductKey
,OrderDateKey
,ShipDateKey
,DueDateKey
,SalesOrganizationKey
,SalesEmployeeKey
,SalesTerritoryKey
,PromotionKey
,CurrencyKey
,LocationKey
,VendorKey
,SaleOrderNumber
,UnitPrice
,UnitCost
,SaleQuantity
,SalePrice
,SaleDiscountAmount
,SaleDiscountPercentage
,NetAmount
,SalesAmount
,CityTax
,CountyTax
,StateTax
,OrderDate
,DueDate
,ShipDate
,POSID
,POSUserID
,ETLID
)
SELECT prod.ProductKey
,dt.DateKey
,dt.DateKey
,dt.DateKey
,org.OganizationKey
,emp.EmployeeKey
,1
,1
,@CURRENCYKEY
,1
,@DATAVENDOR
,sales.Order_Number
,sales.Unit_Price
,0
,sales.QtySold
,sales.Sale_Price
,Sales.Sale_Discount_Amount
,sales.Sale_Discount_Percentage
,sales.Net_Amt
,sales.Sale_Price
,sales.City_Tax
,sales.County_Tax
,sales.State_Tax
,sales.Order_Date
,sales.Order_Date
,sales.Order_Date
,sales.POS_ID
,sales.POS_UserID
,@ETLID
 FROM dbo.['Sales Data$'] sales
LEFT JOIN dbo.DimProduct prod ON prod.ProductAlternateKey = sales.Item_Code
LEFT JOIN dbo.DimDate dt ON dt.Date = CONVERT(date,sales.Order_Date)
LEFT JOIN dbo.DimOrganization  org ON org.OrganizationAlternateKey = sales.Company_ID
LEFT JOIN dbo.DimEmployee emp ON emp.EmployeeNationalIDAlternateKey = sales.POS_UserID


--------------------------FactProductInventory---------------------------------------------------------
PRINT 'Inserting Inventory'

SELECT @DATAVENDOR = VendorKey FROM DimDataVendor 

INSERT INTO dbo.FactProductInventory
(
ProductKey
,DateKey
,VendorKey
,LocationKey
,OrganizationKey
,LotNumber
,BatchNumber
,MovementDate
,UnitCost
,OpenQty
,HoldQty
,BreakageQty
,IntransitQty
,AllocatedQty
,AddQty
,OldQty
,SalesQty
,ETLID
)
SELECT prod.ProductKey
,dt.DateKey
,@DATAVENDOR 
,1
,org.OganizationKey
,inv.Batch_Number
,inv.Lot_Number
,inv.Reporting_date
,inv.Unite_Cost
,inv.open_qty
,inv.hold_qty
,inv.Breakage_qty
,inv.InTransit_qty
,inv.Allocated_qty
,inv.[Add-qty]
,inv.Old_qty
,inv.Sales_qty
,@ETLID

FROM  dbo.['Inventory$'] inv
LEFT JOIN dbo.DimProduct prod ON prod.ProductAlternateKey = inv.Item_Code
LEFT JOIN dbo.DimDate dt ON dt.Date = CONVERT(date,inv.reporting_date)
LEFT JOIN dbo.DimOrganization org ON org.OrganizationAlternateKey = inv.Company_ID







