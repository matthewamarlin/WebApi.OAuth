USE [master]
ALTER DATABASE [JadeDW] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
RESTORE DATABASE [JadeDW] FROM  DISK = N'C:\Users\mdeufel\ITEXACT Limited\OneDrive - ITEXACT Limited\Jade-MD\Model\JadeDW.bak' WITH  FILE = 1,  MOVE N'JadeDW' TO N'C:\Users\mdeufel\SSDTJadeDW_Primary.mdf',  MOVE N'JadeDW_log' TO N'C:\Users\mdeufel\SSDTJadeDW_Primary.ldf',  NOUNLOAD,  REPLACE,  STATS = 5
ALTER DATABASE [JadeDW] SET MULTI_USER

GO


