﻿

CREATE VIEW [dbo].[vwSalesPerOrganization]
AS
select sales.*, org.OrganizationName,org.ProjectedSales from FactSales sales
 JOIN DimOrganization org ON org.OganizationKey = sales.SalesOrganizationKey