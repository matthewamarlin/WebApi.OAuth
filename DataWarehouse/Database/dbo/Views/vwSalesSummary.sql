﻿



CREATE VIEW [dbo].[vwSalesSummary]
AS
select  org.OrganizationName
, org.ProjectedSales
, ISNULL(SUM(sales.NetAmount),0) as SalesTotal  
, CASE WHEN 
	org.ProjectedSales is null THEN null --- null projected sales will return null variance
  WHEN
    org.ProjectedSales = 0 AND ISNULL(SUM(sales.NetAmount),0) != 0 THEN 1 -- zero projected sales will return 100%
  ELSE
    ABS(ISNULL(SUM(sales.NetAmount),0)-org.ProjectedSales)/org.ProjectedSales -- variance = sales-projected/projected 
  END as SalesVariance 
FROM DimOrganization org
LEFT JOIN dbo.FactSales sales ON org.OganizationKey = sales.SalesOrganizationKey
group by org.OrganizationName,org.ProjectedSales