﻿


CREATE VIEW [dbo].[vwInventorySummary]
AS
SELECT org.OrganizationName,
		org.OrganizationAlternateKey,
       prod.ItemName,
	   prod.ProductAlternateKey,
	   inv.DateKey,
OpenQty,
HoldQty,
BreakageQty,
IntransitQty,
AllocatedQty,
AddQty,
OldQty,
SalesQty,
TotalQty,
UnsellableQty,
MovementQty,
	  CASE WHEN ((inv.UnsellableQty >  0) AND (inv.MovementQty != 0))
	  THEN ROUND(CAST( inv.UnsellableQty AS FLOAT) / inv.MovementQty,4)
	  ELSE  0
	   END
	  AS AdjustmentVariance,
	  CASE WHEN ((inv.TotalQty >  0) AND (inv.MovementQty != 0))
	  THEN ROUND( inv.OpenQty+inv.AddQty - (inv.TotalQty+inv.SalesQty+inv.HoldQty+inv.BreakageQty+inv.InTransitQty+inv.AllocatedQty) / CAST( inv.TotalQty AS FLOAT),4)
	  ELSE 0
	  END
    AS InventoryVariance
FROM dbo.DimOrganization org
     JOIN dbo.FactProductInventory inv ON inv.OrganizationKey = org.OganizationKey
     JOIN dbo.DimProduct prod ON prod.ProductKey = inv.ProductKey;