﻿CREATE TABLE [dbo].[DimProduct] (
    [ProductKey]           INT            IDENTITY (1, 1) NOT NULL,
    [ProductAlternateKey]  NVARCHAR (25)  NULL,
    [ItemName]             NVARCHAR (150) NOT NULL,
    [ItemType]             NVARCHAR (64)  NULL,
    [ItemGroupDescription] NVARCHAR (64)  NULL,
    [ItemGroupCategory]    NVARCHAR (64)  NULL,
    [Description]          NVARCHAR (400) NULL,
    [UnitofMeasure]        NVARCHAR (10)  NULL,
    [UnitCost]             MONEY          NULL,
    [UnitPrice]            MONEY          NULL,
    [Size]                 NVARCHAR (50)  NULL,
    [ProdNotes]            NVARCHAR (50)  NULL,
    [SizeRange]            NVARCHAR (50)  NULL,
    [Weight]               FLOAT (53)     NULL,
    [ProductLine]          NCHAR (2)      NULL,
    [DealerPrice]          MONEY          NULL,
    [Class]                NCHAR (2)      NULL,
    [Style]                NCHAR (2)      NULL,
    [ModelName]            NVARCHAR (50)  NULL,
    [StartDate]            DATETIME       CONSTRAINT [DF_DimProduct_StartDate] DEFAULT ((1)) NOT NULL,
    [EndDate]              DATETIME       NOT NULL,
    [CurrentFlag]          BIT            NOT NULL,
    [ETLID]                INT            NOT NULL,
    CONSTRAINT [PK_DimProduct_ProductKey] PRIMARY KEY CLUSTERED ([ProductKey] ASC),
    CONSTRAINT [AK_DimProduct_ProductAlternateKey] UNIQUE NONCLUSTERED ([ProductAlternateKey] ASC)
);







