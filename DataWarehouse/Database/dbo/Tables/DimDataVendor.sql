﻿CREATE TABLE [dbo].[DimDataVendor] (
    [VendorKey]  INT           IDENTITY (1, 1) NOT NULL,
    [VendorName] NVARCHAR (50) NOT NULL,
    [VendorID]   NVARCHAR (30) NOT NULL,
    [ETLID]      INT           NOT NULL,
    CONSTRAINT [PK_DimDataVendor] PRIMARY KEY CLUSTERED ([VendorKey] ASC)
);





