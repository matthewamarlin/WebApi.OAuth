﻿CREATE TABLE [dbo].[DimUnitOfMeasure] (
    [UOMKey]           INT        IDENTITY (1, 1) NOT NULL,
    [UOMDescription]   NCHAR (10) NOT NULL,
    [ConversionFactor] FLOAT (53) NULL,
    [ConversionKey]    INT        NULL,
    CONSTRAINT [PK_DimUnitOfMeasure] PRIMARY KEY CLUSTERED ([UOMKey] ASC),
    CONSTRAINT [FK_DimUnitOfMeasure_DimUnitOfMeasure] FOREIGN KEY ([ConversionKey]) REFERENCES [dbo].[DimUnitOfMeasure] ([UOMKey])
);

