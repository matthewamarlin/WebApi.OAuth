﻿CREATE TABLE [dbo].[DimLocation] (
    [LocationKey]         INT            IDENTITY (1, 1) NOT NULL,
    [LocationID]          NVARCHAR (30)  NOT NULL,
    [LocationDescription] NVARCHAR (100) NULL,
    CONSTRAINT [PK_DimLocation] PRIMARY KEY CLUSTERED ([LocationKey] ASC)
);



