﻿CREATE TABLE [dbo].[DimEmployee] (
    [EmployeeKey]                          INT             IDENTITY (1, 1) NOT NULL,
    [ParentEmployeeKey]                    INT             NULL,
    [EmployeeNationalIDAlternateKey]       NVARCHAR (15)   NULL,
    [ParentEmployeeNationalIDAlternateKey] NVARCHAR (15)   NULL,
    [SalesTerritoryKey]                    INT             NULL,
    [FirstName]                            NVARCHAR (50)   NOT NULL,
    [LastName]                             NVARCHAR (50)   NOT NULL,
    [MiddleName]                           NVARCHAR (50)   NULL,
    [NameStyle]                            BIT             CONSTRAINT [DF_DimEmployee_NameStyle] DEFAULT ((0)) NOT NULL,
    [Title]                                NVARCHAR (50)   NULL,
    [HireDate]                             DATE            NULL,
    [BirthDate]                            DATE            NULL,
    [Phone]                                NVARCHAR (25)   NULL,
    [MaritalStatus]                        NCHAR (1)       NULL,
    [EmergencyContactName]                 NVARCHAR (50)   NULL,
    [EmergencyContactPhone]                NVARCHAR (25)   NULL,
    [Gender]                               NCHAR (1)       NULL,
    [SalesPersonFlag]                      BIT             CONSTRAINT [DF_DimEmployee_SalesPersonFlag] DEFAULT ((0)) NOT NULL,
    [DepartmentName]                       NVARCHAR (50)   NULL,
    [EmployeePhoto]                        VARBINARY (MAX) NULL,
    [StartDate]                            DATE            NULL,
    [EndDate]                              DATE            NULL,
    [CurrentFlag]                          BIT             CONSTRAINT [DF_DimEmployee_CurrentFlag] DEFAULT ((1)) NOT NULL,
    [ETLID]                                NCHAR (10)      NULL,
    CONSTRAINT [PK_DimEmployee_EmployeeKey] PRIMARY KEY CLUSTERED ([EmployeeKey] ASC),
    CONSTRAINT [FK_DimEmployee_DimEmployee] FOREIGN KEY ([ParentEmployeeKey]) REFERENCES [dbo].[DimEmployee] ([EmployeeKey]),
    CONSTRAINT [FK_DimEmployee_DimSalesTerritory] FOREIGN KEY ([SalesTerritoryKey]) REFERENCES [dbo].[DimSalesTerritory] ([SalesTerritoryKey])
);









