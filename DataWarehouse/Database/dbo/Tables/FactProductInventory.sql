﻿CREATE TABLE [dbo].[FactProductInventory] (
    [InventoryKey]    INT           IDENTITY (1, 1) NOT NULL,
    [ProductKey]      INT           NOT NULL,
    [DateKey]         INT           NOT NULL,
    [VendorKey]       INT           NOT NULL,
    [LocationKey]     INT           NOT NULL,
    [OrganizationKey] INT           NOT NULL,
    [LotNumber]       NVARCHAR (30) NULL,
    [BatchNumber]     NVARCHAR (30) NULL,
    [MovementDate]    DATE          NOT NULL,
    [UnitCost]        MONEY         NULL,
    [OpenQty]         INT           CONSTRAINT [DF_FactProductInventory_OpenQty] DEFAULT ((0)) NOT NULL,
    [HoldQty]         INT           CONSTRAINT [DF_FactProductInventory_HoldQty] DEFAULT ((0)) NOT NULL,
    [BreakageQty]     INT           CONSTRAINT [DF_FactProductInventory_BreakageQty] DEFAULT ((0)) NOT NULL,
    [IntransitQty]    INT           CONSTRAINT [DF_FactProductInventory_IntransitQty] DEFAULT ((0)) NOT NULL,
    [AllocatedQty]    INT           CONSTRAINT [DF_FactProductInventory_AllocatedQty] DEFAULT ((0)) NOT NULL,
    [AddQty]          INT           CONSTRAINT [DF_FactProductInventory_AddQty] DEFAULT ((0)) NOT NULL,
    [OldQty]          INT           CONSTRAINT [DF_FactProductInventory_OldQty] DEFAULT ((0)) NOT NULL,
    [SalesQty]        INT           CONSTRAINT [DF_FactProductInventory_SalesQty] DEFAULT ((0)) NOT NULL,
    [TotalQty]        AS            (([OldQty]+[AddQty])-(((([SalesQty]+[AllocatedQty])+[IntransitQty])+[BreakageQty])+[HoldQty])) PERSISTED,
    [UnsellableQty]   AS            ((([HoldQty]+[BreakageQty])+[IntransitQty])+[AllocatedQty]) PERSISTED,
    [MovementQty]     AS            (([OpenQty]+[AddQty])-(((((([OldQty]+[AddQty])+[SalesQty])+[AllocatedQty])+[IntransitQty])+[BreakageQty])+[HoldQty])) PERSISTED,
    [ETLID]           INT           NOT NULL,
    CONSTRAINT [PK_FactProductInventory] PRIMARY KEY CLUSTERED ([InventoryKey] ASC),
    CONSTRAINT [FK_FactProductInventory_DimDataVendor] FOREIGN KEY ([VendorKey]) REFERENCES [dbo].[DimDataVendor] ([VendorKey]),
    CONSTRAINT [FK_FactProductInventory_DimDate] FOREIGN KEY ([DateKey]) REFERENCES [dbo].[DimDate] ([DateKey]),
    CONSTRAINT [FK_FactProductInventory_DimLocation] FOREIGN KEY ([LocationKey]) REFERENCES [dbo].[DimLocation] ([LocationKey]),
    CONSTRAINT [FK_FactProductInventory_DimOrganization] FOREIGN KEY ([OrganizationKey]) REFERENCES [dbo].[DimOrganization] ([OganizationKey]),
    CONSTRAINT [FK_FactProductInventory_DimProduct] FOREIGN KEY ([ProductKey]) REFERENCES [dbo].[DimProduct] ([ProductKey])
);










GO
CREATE NONCLUSTERED INDEX [IX_Factproduct_1]
    ON [dbo].[FactProductInventory]([OrganizationKey] ASC)
    INCLUDE([ProductKey], [OpenQty], [HoldQty], [BreakageQty], [IntransitQty], [AllocatedQty], [AddQty], [OldQty]);

