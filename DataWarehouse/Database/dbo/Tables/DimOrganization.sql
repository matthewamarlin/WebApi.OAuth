﻿CREATE TABLE [dbo].[DimOrganization] (
    [OganizationKey]            INT            IDENTITY (1, 1) NOT NULL,
    [OrganizationAlternateKey]  NVARCHAR (15)  NOT NULL,
    [ParentOrganizationKey]     INT            NULL,
    [PercentageOfOwnership]     NVARCHAR (20)  NULL,
    [OrganizationName]          NVARCHAR (50)  NOT NULL,
    [OrganizationDescription]   NVARCHAR (150) NOT NULL,
    [OrganizationCategory]      NVARCHAR (60)  NOT NULL,
    [OrganizationStreetAddress] NVARCHAR (100) NULL,
    [OrganizationCity]          NVARCHAR (100) NULL,
    [OrganizationState]         NVARCHAR (100) NULL,
    [ProjectedSales]            FLOAT (53)     CONSTRAINT [DF_DimOrganization_ProjectedSales] DEFAULT ((0)) NOT NULL,
    [CurrencyKey]               INT            NOT NULL,
    [StartDate]                 DATETIME       NOT NULL,
    [EndDate]                   DATETIME       NOT NULL,
    [CurrentFlag]               BIT            CONSTRAINT [DF_DimOrganization_CurrentFlag] DEFAULT ((0)) NOT NULL,
    [ETLID]                     INT            NOT NULL,
    CONSTRAINT [PK_DimOrganization] PRIMARY KEY CLUSTERED ([OganizationKey] ASC),
    CONSTRAINT [FK_DimOrganization_DimOrganization] FOREIGN KEY ([ParentOrganizationKey]) REFERENCES [dbo].[DimOrganization] ([OganizationKey])
);













