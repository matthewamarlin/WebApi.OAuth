﻿CREATE TABLE [dbo].[FactSales] (
    [ProductKey]             INT           NOT NULL,
    [OrderDateKey]           INT           NOT NULL,
    [DueDateKey]             INT           NOT NULL,
    [ShipDateKey]            INT           NOT NULL,
    [SalesOrganizationKey]   INT           NOT NULL,
    [SalesEmployeeKey]       INT           NULL,
    [SalesTerritoryKey]      INT           NOT NULL,
    [PromotionKey]           INT           NOT NULL,
    [CurrencyKey]            INT           NOT NULL,
    [LocationKey]            INT           NOT NULL,
    [VendorKey]              INT           NOT NULL,
    [SaleOrderNumber]        NVARCHAR (20) NOT NULL,
    [UnitPrice]              MONEY         NOT NULL,
    [UnitCost]               MONEY         NOT NULL,
    [SaleQuantity]           SMALLINT      NOT NULL,
    [SalePrice]              MONEY         NOT NULL,
    [SaleDiscountAmount]     FLOAT (53)    NOT NULL,
    [SaleDiscountPercentage] FLOAT (53)    NOT NULL,
    [NetAmount]              MONEY         NOT NULL,
    [SalesAmount]            MONEY         NOT NULL,
    [CityTax]                MONEY         NULL,
    [CountyTax]              MONEY         NULL,
    [StateTax]               MONEY         NULL,
    [Freight]                MONEY         NULL,
    [OrderDate]              DATETIME      NULL,
    [DueDate]                DATETIME      NULL,
    [ShipDate]               DATETIME      NULL,
    [POSID]                  NVARCHAR (30) NOT NULL,
    [POSUserID]              NVARCHAR (50) NULL,
    [ETLID]                  INT           NOT NULL,
    CONSTRAINT [PK_FactSales] PRIMARY KEY CLUSTERED ([SaleOrderNumber] ASC),
    CONSTRAINT [FK_FactSales_DimCurrency] FOREIGN KEY ([CurrencyKey]) REFERENCES [dbo].[DimCurrency] ([CurrencyKey]),
    CONSTRAINT [FK_FactSales_DimDataVendor] FOREIGN KEY ([VendorKey]) REFERENCES [dbo].[DimDataVendor] ([VendorKey]),
    CONSTRAINT [FK_FactSales_DimDate] FOREIGN KEY ([OrderDateKey]) REFERENCES [dbo].[DimDate] ([DateKey]),
    CONSTRAINT [FK_FactSales_DimDate1] FOREIGN KEY ([ShipDateKey]) REFERENCES [dbo].[DimDate] ([DateKey]),
    CONSTRAINT [FK_FactSales_DimDate2] FOREIGN KEY ([DueDateKey]) REFERENCES [dbo].[DimDate] ([DateKey]),
    CONSTRAINT [FK_FactSales_DimEmployee] FOREIGN KEY ([SalesEmployeeKey]) REFERENCES [dbo].[DimEmployee] ([EmployeeKey]),
    CONSTRAINT [FK_FactSales_DimLocation] FOREIGN KEY ([LocationKey]) REFERENCES [dbo].[DimLocation] ([LocationKey]),
    CONSTRAINT [FK_FactSales_DimOrganization] FOREIGN KEY ([SalesOrganizationKey]) REFERENCES [dbo].[DimOrganization] ([OganizationKey]),
    CONSTRAINT [FK_FactSales_DimProduct] FOREIGN KEY ([ProductKey]) REFERENCES [dbo].[DimProduct] ([ProductKey]),
    CONSTRAINT [FK_FactSales_DimPromotion] FOREIGN KEY ([PromotionKey]) REFERENCES [dbo].[DimPromotion] ([PromotionKey]),
    CONSTRAINT [FK_FactSales_DimSalesTerritory] FOREIGN KEY ([SalesTerritoryKey]) REFERENCES [dbo].[DimSalesTerritory] ([SalesTerritoryKey])
);











