﻿CREATE TABLE [dbo].[DimProductSubcategory] (
    [ProductSubcategoryKey]          INT           IDENTITY (1, 1) NOT NULL,
    [ProductSubcategoryAlternateKey] NVARCHAR (50) NULL,
    [ProductSubcategoryName]         NVARCHAR (50) NOT NULL,
    [ProductCategoryKey]             INT           NULL,
    CONSTRAINT [PK_DimProductSubcategory_ProductSubcategoryKey] PRIMARY KEY CLUSTERED ([ProductSubcategoryKey] ASC),
    CONSTRAINT [FK_DimProductSubcategory_DimProductCategory] FOREIGN KEY ([ProductCategoryKey]) REFERENCES [dbo].[DimProductCategory] ([ProductCategoryKey]),
    CONSTRAINT [AK_DimProductSubcategory_ProductSubcategoryAlternateKey] UNIQUE NONCLUSTERED ([ProductSubcategoryAlternateKey] ASC)
);



