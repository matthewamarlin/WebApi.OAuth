
--Prepare Dimensions
DECLARE @ENDDATE DATETIME,
       @STARTDATE DATETIME,
	   @CURRENCYKEY INT,
	   @DATAVENDOR INT,
	   @ETLID INT

SET @STARTDATE = GETDATE()
SET @ENDDATE = '9999-01-01'
SET @ETLID =1

DELETE FROM dbo.FactSales
DELETE FROM dbo.DimProduct
DELETE FROM dbo.DimEmployee
DELETE FROM dbo.DimOrganization
DELETE FROM dbo.DimCurrency
DELETE FROM dbo.DimDataVendor

--------------------------DimCUrrency---------------------------------------------------------

INSERT INTO dbo.DimCurrency (CurrencyAlternateKey,CurrencyName)
VALUES('USD','US Dollar')

--------------------------DimDate---------------------------------------------------------
--Done with script DIMDatePopulate.sql

--------------------------DimDatavendor---------------------------------------------------------

INSERT INTO dbo.DimDataVendor (VendorName, VendorID, ETLID)
VALUES ('Patrick Excel Sample data','PAT',1)


--------------------------DimPRODUCT---------------------------------------------------------

INSERT INTO dbo.DimProduct (ProductAlternateKey, ItemName, Description, UnitPrice, Startdate, EndDate,CurrentFlag,ETLID )
SELECT DISTINCT Item_Code,ItemName,Description,Unit_Price,@STARTDATE, @ENDDATE,1,1 FROM
dbo.['Sales Detail$']

-------------------------DimEmployee--------------------------------------------------------

INSERT INTO dbo.DimEmployee 
(
ParentEmployeeKey
,EmployeeNationalIDAlternateKey
,ParentEmployeeNationalIDAlternateKey
,SalesTerritoryKey
,FirstName
,LastName
,MiddleName
,NameStyle
,Title
,HireDate
,BirthDate
,Phone
,MaritalStatus
,EmergencyContactName
,EmergencyContactPhone
,Gender
,SalesPersonFlag
,DepartmentName
,EmployeePhoto
,StartDate
,EndDate
,CurrentFlag
,ETLID)
SELECT 
null
,e.Personal_SSN
,null
,1
,e.Personal_FirstName
,e.Personal_MiddleName
,e.Personal_LastName
,0
,e.Personal_Prefix
,null
,null
,e.Business_Phone
,null
,null
,null
,null
,1
,null
,null
,@STARTDATE
,@ENDDATE
,1
,@ETLID
FROM dbo.['Employee List$'] e

-------------------------DIMOrganization--------------------------------------------------------

SELECT @CURRENCYKEY = CurrencyKey FROM DimCurrency WHERE CurrencyAlternateKey = 'USD'

INSERT INTO dbo.DimOrganization (OrganizationAlternateKey,OrgnizationName,OrganizationDescription,OrganizationCategory,CurrencyKey,StartDate,EndDate,CurrentFlag,ETLID)
SELECT DISTINCT Company_ID,Business_CompanyName, Business_Description,Business_Category ,@CURRENCYKEY,@STARTDATE,@ENDDATE,1,@ETLID
FROM dbo.['Client List$']

---------------------------------Fact Sales------------------------------------------------

SELECT @CURRENCYKEY = CurrencyKey FROM DimCurrency WHERE CurrencyAlternateKey = 'USD'
SELECT @DATAVENDOR = VendorKey FROM DimDataVendor 

--Prepare FACTs
TRUNCATE TABLE dbo.FactSales

INSERT INTO dbo.FactSales 
(
ProductKey
,OrderDateKey
,ShipDateKey
,DueDateKey
,SalesOrganizationKey
,SalesEmployeeKey
,SalesTerritoryKey
,PromotionKey
,CurrencyKey
,LocationKey
,VendorKey
,SaleOrderNumber
,UnitPrice
,UnitCost
,SaleQuantity
,SalePrice
,SaleDiscountAmount
,SaleDiscountPercentage
,NetAmount
,SalesAmount
,CityTax
,CountyTax
,StateTax
,OrderDate
,DueDate
,ShipDate
,POSID
,POSUserID
,ETLID
)
SELECT prod.ProductKey
,dt.DateKey
,dt.DateKey
,dt.DateKey
,org.OganizationKey
,emp.EmployeeKey
,1
,1
,@CURRENCYKEY
,1
,@DATAVENDOR
,sales.Order_Number
,sales.Unit_Price
,0
,sales.QtySold
,sales.Sale_Price
,Sales.Sale_Discount_Amount
,sales.Sale_Discount_Percentage
,sales.Net_Amt
,sales.Sale_Price
,sales.City_Tax
,sales.County_Tax
,sales.State_Tax
,sales.Order_Date
,sales.Order_Date
,sales.Order_Date
,sales.POS_ID
,sales.POS_UserID
,@ETLID
 FROM dbo.['Sales Detail$'] sales
LEFT JOIN dbo.DimProduct prod ON prod.ProductAlternateKey = sales.Item_Code
LEFT JOIN dbo.DimDate dt ON dt.Date = CONVERT(date,sales.Order_Date)
LEFT JOIN dbo.DimOrganization  org ON org.OrganizationAlternateKey = sales.Company_ID
LEFT JOIN dbo.DimEmployee emp ON emp.EmployeeNationalIDAlternateKey = sales.POS_UserID






