﻿namespace SpearOne.Examples.OAuth.Service.Contracts
{
    public interface IChatHubClient
    {
        void ReceiveMessage(string sender, string message);
    }
}
