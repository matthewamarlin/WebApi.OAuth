﻿namespace SpearOne.Examples.OAuth.Service.Contracts
{
    public interface IChatHub
    {
        void Send(string sender, string message);
    }
}
