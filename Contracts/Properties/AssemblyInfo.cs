﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.OAuth.Service.Contracts")]
[assembly: AssemblyDescription("A class library containing the common Types used for communicating with the SpearOne.Example.OAuth.Service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("DDA667BF-8D89-462E-8786-54E00889A95D")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]