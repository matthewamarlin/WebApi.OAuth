# WebApi OAuth Service

This is an example OAuth service written using WebApi and the ASP.NET Identity Framework

## Software life cycle
- [GitFlow] - This project uses git flow branching model

## How to get started

### Prerequisites
- [Visual Studio 2017][Visual Studio Downloads] - Community Edition or Higher
- [PowerShell Tools for Visual Studio 2017] - This will enable you to edit the powershell `Scripts` project.

### NuGet Packages

This project depends on this [Common Library], which is available as a NuGet package on my public [NuGet Server]

### Quick Start Scripts

Scripts have been included to assist with building, debugging and deploying the application.
- `Build.cmd` - Builds the project
- `Clean.cmd` - Removes all generated build artefacts

### Debugging the project

1. Open **Service.sln** and ensure that the **Service.Host** Project is set as the startup project.
2. Startup the debugger and the service will automatically host within a console application.

## Authors
- Matthew Marlin 

-----------------------------
[Visual Studio Downloads]: https://www.visualstudio.com/downloads
[Nancy]: http://nancyfx.org/
[SignalR]: https://www.asp.net/signalr
[PowerShell Tools for Visual Studio 2017]: https://marketplace.visualstudio.com/items?itemName=AdamRDriscoll.PowerShellToolsforVisualStudio2017-18561
[GitFlow]: https://datasift.github.io/gitflow/IntroducingGitFlow.html
[Common Library]: https://gitlab.com/matthewamarlin/Common
[NuGet Server]: http://nuget.spearone.net/