﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Threading;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using log4net.Config;

namespace SpearOne.Examples.OAuth.Service.Host
{
    public class Startup
    {
        private static readonly Log4NetWrapper Log = new Log4NetWrapper(typeof(Startup));

        #region Console Close CtrlEventHandler

        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(CtrlEventHandler handler, bool add);

        public delegate bool CtrlEventHandler(CtrlType sig);

        private static CtrlEventHandler _handler;

        public enum CtrlType
        {
            CtrlCEvent = 0,
            CtrlBreakEvent = 1,
            CtrlCloseEvent = 2,
            CtrlLogoffEvent = 5,
            CtrlShutdownEvent = 6
        }

        private static bool OnConsoleExitHandler(CtrlType sig)
        {
            Console.WriteLine(@"Exited due to Ctrl+C or process killed");

            ServiceHost.StopService();
            
            // Shutdown right away so there are no lingering threads
            // Environment.Exit(0);

            return true;
        }

        #endregion

        #region Start Methods

        public static void StartConsoleHost()
        {
            try
            {
                ServiceHost.StartService();

                Console.WriteLine(@"Press Ctrl+C to Stop");

                Thread.Sleep(Timeout.Infinite);
            }
            catch
            {
                Console.ReadLine();
            }
        }

        public static void StartServiceHost()
        {
            try
            {
                var servicesToRun = new ServiceBase[] { new ServiceHost(), };
                ServiceBase.Run(servicesToRun);
            }
            catch (Exception ex)
            {
                Log.WriteFormat(Level.Error, "An error occured: \n{0}", ex);
            }
        }

        #endregion

        private static void Main(string[] args)
        {
            _handler += OnConsoleExitHandler;
            SetConsoleCtrlHandler(_handler, true);

            XmlConfigurator.Configure();

            var isRunningInConsole = Console.OpenStandardInput(1) != Stream.Null;

            if (isRunningInConsole)
            {
                Log.WriteFormat(Level.Info, "Starting in Console Mode");
                StartConsoleHost();
            }
            else
            {
                Log.WriteFormat(Level.Info, "Starting in Service Mode");
                StartServiceHost();
            }
        }
    }
}