﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.OAuth.Service.Host")]
[assembly: AssemblyDescription("The SpearOne.Examples.OAuth.Service, hosted as either a console application or windows service.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("85A3DDE6-1942-43A3-A0FC-70EF38E21245")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]