﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace SpearOne.Examples.OAuth.Service.Host
{
    [RunInstaller(true)]
    public partial class ServiceHostInstaller : Installer
    {
        public ServiceHostInstaller()
        {
            Install();
        }

        public void Install()
        {
            Installers.Add(new ServiceInstaller
            {
                ServiceName = "SpearOne.Examples.OAuth.Service",
                DisplayName = "SpearOne - Example OAuth Service",
                Description = "An example OAuth service - using ASP.NET Identity Framework and WebAPI",
                StartType = ServiceStartMode.Automatic
            });

            Installers.Add(new ServiceProcessInstaller { Account = ServiceAccount.LocalSystem });
        }
    }
}
