﻿using System;
using System.ServiceProcess;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using Microsoft.Owin.Hosting;

namespace SpearOne.Examples.OAuth.Service.Host
{
    public partial class ServiceHost : ServiceBase
    {
        private static readonly Log4NetWrapper Log = new Log4NetWrapper(typeof(ServiceHost));
        private static IDisposable _webApp;

        public ServiceHost()
        {
            InitializeComponent();
        }

        #region Overrides of ServiceBase

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            StartService(args);
        }

        protected override void OnStop()
        {
            StopService();
            base.OnStop();
        }

        #endregion

        public static void StartService(string[] args = null)
        {
            try
            {
                var options = new StartOptions();
                options.Urls.Add("http://localhost:4205/");
                options.Urls.Add("http://oauth-service.examples.spearone.net:4205/");

                _webApp = WebApp.Start(options);

                Log.Write(Level.Info, $"{typeof(ServiceHost)} Started");
                Log.Write(Level.Info, $"Server Bindings: \n -> {string.Join(Environment.NewLine + " -> ", options.Urls)}");
            }
            catch (Exception exception)
            {
                Log.Write(Level.Fatal, exception);
                throw;
            }
        }

        public static void StopService()
        {
            Log.Write(Level.Info, "Service is attempting to stop...");

            try
            {
                _webApp.Dispose();
            }
            catch (Exception exception)
            {
                Log.Write(Level.Fatal, exception);
                throw;
            }

            Log.Write(Level.Info, "Service stopped!");
        }

        public static void RestartService()
        {
            StopService();
            StartService();
        }
    }
}
