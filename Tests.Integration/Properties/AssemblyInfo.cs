using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.OAuth.Service.Tests.Integration")]
[assembly: AssemblyDescription("This assembly contains all the Integration test logic for the SpearOne.Examples.OAuth.Service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("624E28FE-B9F7-498F-AB66-5E358C6F706C")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]