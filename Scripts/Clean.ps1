Push-Location
Set-Location $PSScriptRoot

$List = @("../packages", 
          "../Website/node_modules",
          "../Website/dist",
          "../Website/documentation")

# Delete specifically mentioned items
$List | foreach {
    try { 
        Remove-Item -Path $_ -Recurse -Force -ErrorAction stop
        Write-Host "Removed '$_'"
    } catch { 
        if(($_.ToString() -like "Cannot find path") -or ($_.ToString() -like "Could not find part of the path")){
            throw $_
        }
    }
}

# Find & delete any bin / obj folders remaining
Get-ChildItem -Path ..\ -Recurse | foreach {

    if(($_.Name -eq "bin") -or ($_.Name -eq "obj"))
    {
        Remove-Item -Path "$($_.FullName)\" -Recurse -Force
        Write-Host "Removed '$($_.FullName)'"
    }
}

Pop-Location