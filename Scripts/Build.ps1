$MSBuildLocation = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\amd64\MSBuild.exe"
$SolutionName = "../Service.sln"
$MSBuildCommands = "/target:Rebuild /p:RunOctoPack=true"

try 
{ 
    $ErrorActionPreference = "Stop"

    Push-Location
    Set-Location $PSScriptRoot

	nuget restore $SolutionName
    iex "& `"$MSBuildLocation`" $SolutionName $MSBuildCommands"
} 
catch 
{
    Write-Error $_
}
finally {
    Pop-Location
}