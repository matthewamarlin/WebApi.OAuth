﻿using System.Reflection;

[assembly: AssemblyCompany("SpearOne")]
[assembly: AssemblyProduct("SpearOne.Examples.OAuth.Service")]
[assembly: AssemblyCopyright("Copyright © SpearOne 2017")]
[assembly: AssemblyTrademark("Tramemark ™ SpearOne 2017")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyInformationalVersion("1.0.0")]