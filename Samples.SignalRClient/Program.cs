using System;
using System.Runtime.InteropServices;
using System.Threading;
using SpearOne.Examples.OAuth.Service.Proxy;

namespace SpearOne.Examples.OAuth.Service.Samples.SignalRClient
{
    public class Program
    {
        private readonly ChatProxy _chatProxy;

        public string TempDirectory { get; set; }

        public Program()
        {
            //ReadConfig();
            _handler += OnConsoleExitHandler;
            SetConsoleCtrlHandler(_handler, true);

            _chatProxy = new ChatProxy();
            _chatProxy.ReceivedMessageEvent += OnReceiveMessage;
        }
        
        public void Start()
        {
            string line;
            while ((line = Console.ReadLine()) != null)
            {
                _chatProxy.Send("Console App", line);
            }

            Console.ReadLine();
        }

        private void OnReceiveMessage(object sender, string message)
        {
            Console.WriteLine(message);
        }

        #region Console CtrlEventHandler pInvoke

        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(CtrlEventHandler handler, bool add);

        public delegate bool CtrlEventHandler(CtrlType sig);

        private static CtrlEventHandler _handler;

        public enum CtrlType
        {
            CtrlCEvent = 0,
            CtrlBreakEvent = 1,
            CtrlCloseEvent = 2,
            CtrlLogoffEvent = 5,
            CtrlShutdownEvent = 6
        }
        #endregion

        private static bool OnConsoleExitHandler(CtrlType sig)
        {
            Console.WriteLine("Exiting system due to external CTRL-C, or process kill, or shutdown");

            //TODO Clean up

            Console.WriteLine("Cleanup complete");

            //shutdown right away so there are no lingering threads
            Environment.Exit(-1);

            return true;
        }

        public static void Main(string[] args)
        {
            Thread.Sleep(5000);

            var consoleApp = new Program();
            consoleApp.Start();
        }
    }
}
