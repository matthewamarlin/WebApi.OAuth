﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.OAuth.Samples.SignalRClient")]
[assembly: AssemblyDescription("This is an example SignalR client that interacts with the SpearOne.Examples.OAuth.Service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("7F6D190F-45B1-4E0B-90D5-2FB78D4432CE")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]