﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using SpearOne.Examples.OAuth.Service.Core.Entities;

namespace SpearOne.Examples.OAuth.Service.Core.Identity
{
    public class AuthDbContext : IdentityDbContext<ApplicationUser>
    {
        public AuthDbContext()
            : base("AuthDbContext", throwIfV1Schema: true)
        {
            //Configuration.ProxyCreationEnabled = false;
            //Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        #region Helpers

        public static AuthDbContext Create()
        {
            return new AuthDbContext();
        }

        #endregion
    }
}