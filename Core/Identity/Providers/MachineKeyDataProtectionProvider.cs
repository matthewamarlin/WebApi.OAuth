﻿using Microsoft.Owin.Security.DataProtection;

namespace SpearOne.Examples.OAuth.Service.Core.Identity.Providers
{
    internal class MachineKeyProtectionProvider : IDataProtectionProvider
    {
        public virtual IDataProtector Create(params string[] purposes)
        {
            return new MachineKeyDataProtector(purposes);
        }
    }
}