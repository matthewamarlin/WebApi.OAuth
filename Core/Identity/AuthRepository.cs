﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpearOne.Examples.OAuth.Service.Core.Entities;

namespace SpearOne.Examples.OAuth.Service.Core.Identity
{
    /// <summary>
    ///     Simple repository for accessing the authentication section of the database (This wraps functionality that can be found in ApplicationUserManager)
    /// </summary>
    public class AuthRepository : IDisposable
    {
        private readonly AuthDbContext _dbContext;

        public AuthRepository()
        {
            _dbContext = new AuthDbContext();
        }

        public Client FindClient(string clientId)
        {
            var client = _dbContext.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

            var existingToken = _dbContext.RefreshTokens.SingleOrDefault(r => r.Subject == token.Subject && r.ClientId == token.ClientId);

            if (existingToken != null)
            {
                await RemoveRefreshToken(existingToken);
            }

            _dbContext.RefreshTokens.Add(token);

            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _dbContext.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken == null) return false;

            _dbContext.RefreshTokens.Remove(refreshToken);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _dbContext.RefreshTokens.Remove(refreshToken);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            return await _dbContext.RefreshTokens.FindAsync(refreshTokenId);
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return _dbContext.RefreshTokens.ToList();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}