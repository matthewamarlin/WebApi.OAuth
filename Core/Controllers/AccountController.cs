﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using SpearOne.Examples.OAuth.Service.Core.Identity;
using SpearOne.Examples.OAuth.Service.Core.Identity.Providers;
using SpearOne.Examples.OAuth.Service.Core.Models;
using SpearOne.Examples.OAuth.Service.Core.Results;

namespace SpearOne.Examples.OAuth.Service.Core.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : IdentityApiController
    {
        private const string LocalLoginProvider = "Local";
        private readonly AuthRepository _authRepo;
        private readonly ConfigReader _configReader;
        private readonly ApplicationUserManager _applicationUserManager;
        private readonly ApplicationRoleManager _applicationRoleManager;

        private readonly ISecureDataFormat<AuthenticationTicket> _accessTokenFormat;
        private IAuthenticationManager Authentication => Request.GetOwinContext().Authentication;

        public AccountController(AuthRepository authRepository,
            ConfigReader configReader,
            ApplicationUserManager applicationUserManager,
            ApplicationRoleManager applicationRoleManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            _authRepo = authRepository;
            _configReader = configReader;
            _applicationUserManager = applicationUserManager;
            _applicationRoleManager = applicationRoleManager;
            _accessTokenFormat = accessTokenFormat;
        }

        #region Anonymous

        // POST api/Account/Create
        [AllowAnonymous]
        [Route("create")]
        public async Task<IHttpActionResult> Create(CreateUserModel model)
        {
            //TODO Ignore Roles? Or remove them from model?

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                UserName = model.Email, //model.Username,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                JoinDate = DateTime.Now.Date,
            };

            var addUserResult = await _applicationUserManager.CreateAsync(user, model.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

            //Email Confirmation
            var token = await _applicationUserManager.GenerateEmailConfirmationTokenAsync(user.Id);

            //This is a callback to the API
            var callbackUri = new Uri(Url.Link("ConfirmAccountRoute", new { id = user.Id, token })).ToString();

            if (!string.IsNullOrWhiteSpace(model.RedirectUri))
            {
                callbackUri = $"{model.RedirectUri}?id={user.Id}&token={token}";
            }

            //Callback to website?
            //var callbackUrl = $"{_configReader.WebsiteUrl}{_configReader.WebsiteRoutesConfirmAccount}?id={user.Id}&token={token}";
            try
            {

                await _applicationUserManager.SendEmailAsync(user.Id,
                    "Confirm your account",
                    "Please confirm your account by clicking <a href=\"" + callbackUri +
                    "\">here</a>"); //TODO Configurable?

            }
            catch (Exception e)
            {
                var d = e;
            }

            var locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            return Created(locationHeader, CreateUserModel(user));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("Confirm", Name = "ConfirmAccountRoute")]
        public async Task<IHttpActionResult> Confirm(string id = null, string token = null)
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(token))
            {
                ModelState.AddModelError("validation_error", "Id and Token are required.");
                return BadRequest(ModelState);
            }

            var user = await _applicationUserManager.FindByIdAsync(id);
            if (user == null)
            {
                ModelState.AddModelError("validation_error", "User does not exist.");
                return BadRequest(ModelState);
            }

            if (user.EmailConfirmed)
            {
                ModelState.AddModelError("validation_error", "Account already confirmed.");
                return BadRequest(ModelState);
            }

            var result = await _applicationUserManager.ConfirmEmailAsync(id, token);
            
            return result.Succeeded ? Ok("Your account has been confirmed sucessfully.") : GetErrorResult(result);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("ObtainLocalAccessToken")]
        public async Task<IHttpActionResult> ObtainLocalAccessToken(string provider, string externalAccessToken)
        {

            if (string.IsNullOrWhiteSpace(provider) || string.IsNullOrWhiteSpace(externalAccessToken))
            {
                return BadRequest("Provider or external access token is not sent");
            }

            var verifiedAccessToken = await VerifyExternalAccessToken(provider, externalAccessToken);
            if (verifiedAccessToken == null)
            {
                return BadRequest("Invalid Provider or External Access Token");
            }

            var user = await _applicationUserManager.FindAsync(new UserLoginInfo(provider, verifiedAccessToken.user_id));

            var hasRegistered = user != null;

            if (!hasRegistered)
            {
                return BadRequest("External user is not registered");
            }

            //generate access token response
            var accessTokenResponse = GenerateLocalAccessTokenResponse(user.UserName);

            return Ok(accessTokenResponse);
        }

        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = _applicationUserManager.FindByEmail(model.EmailAddress);

            if (user == null || !await _applicationUserManager.IsEmailConfirmedAsync(user.Id))
            {
                //Dont reveal that the user doesnt exist or that the email wasnt confirmed.
                return BadRequest("Your request did not return any results. Please try again with correct information.");
            }

            var resetToken = await _applicationUserManager.GeneratePasswordResetTokenAsync(user.Id);
            var redirectUri = $"{model.RedirectUri}?id={user.Id}&token={resetToken}";

            await _applicationUserManager.SendEmailAsync(user.Id, "Reset your password", $"<a href='{redirectUri}'>Reset Password</a>");

            return Ok();
        }

        [AllowAnonymous]
        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _applicationUserManager.ResetPasswordAsync(model.Id, model.Token, model.Password);

            return result.Succeeded ? Ok() : GetErrorResult(result);
        }

        #endregion

        #region Authorized

        [Authorize(Roles = "Admin")]
        [Route("users")]
        public IHttpActionResult GetUsers()
        {
            //Only SuperAdmin or Admin can delete users (Later when implement roles)
            return Ok(_applicationUserManager.Users.ToList().Select(CreateUserModel));
        }

        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            //Only SuperAdmin or Admin can delete users (Later when implement roles)
            var user = await _applicationUserManager.FindByIdAsync(id);

            if (user != null)
            {
                return Ok(CreateUserModel(user));
            }

            return NotFound();
        }

        [Authorize(Roles = "Admin")]
        [Route("user/{username}")]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            var user = await _applicationUserManager.FindByNameAsync(username);

            if (user != null)
            {
                return Ok(CreateUserModel(user));
            }

            return NotFound();
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)] //TODO????
        [Route("info")]
        public UserInfoModel GetCurrentUsersInfo()
        {
            var userId = User.Identity.GetUserId();
            var externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);
            var user = _applicationUserManager.FindById(userId);
            

            return new UserInfoModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                JoinDate = user.JoinDate,
                Email = User.Identity.GetUserName(),
                HasLocalAccount = true,
                LoginProvider = externalLogin?.LoginProvider
            };
        }
        
        [Authorize]
        [Route("logout")]
        [HttpGet]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);

            var ident = User.Identity as ClaimsIdentity;

            //TODO Invalidate token on server side.

            return Ok();
        }

        //[Route("ManageInfo")]
        //public async Task<ManageInfoModel> GetManageInfo(string returnUrl, bool generateState = false)
        //{
        //    var user = await _applicationUserManager.FindByIdAsync(User.Identity.GetUserId());

        //    if (user == null)
        //    {
        //        return null;
        //    }

        //    var logins = new List<UserLoginInfoModel>();

        //    foreach (var linkedAccount in user.Logins)
        //    {
        //        logins.Add(new UserLoginInfoModel
        //        {
        //            LoginProvider = linkedAccount.LoginProvider,
        //            ProviderKey = linkedAccount.ProviderKey
        //        });
        //    }

        //    if (user.PasswordHash != null)
        //    {
        //        logins.Add(new UserLoginInfoModel
        //        {
        //            LoginProvider = LocalLoginProvider,
        //            ProviderKey = user.UserName,
        //        });
        //    }

        //    return new ManageInfoModel
        //    {
        //        LocalLoginProvider = LocalLoginProvider,
        //        Email = user.UserName,
        //        Logins = logins,
        //        ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
        //    };
        //}

        [Authorize]
        [Route("changePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _applicationUserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            return !result.Succeeded ? GetErrorResult(result) : Ok();
        }

        [Authorize(Roles = "Admin")]
        [Route("setPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_applicationUserManager.Users.Any(x => x.Id == model.UserId))
            {
                return NotFound();
            }

            var token = await _applicationUserManager.GeneratePasswordResetTokenAsync(model.UserId);
            var result = await _applicationUserManager.ResetPasswordAsync(model.UserId, token, model.NewPassword);

            return !result.Succeeded ? GetErrorResult(result) : Ok();
        }
        
        [Authorize(Roles = "Admin")]
        [Route("delete/{id:guid}")]
        public async Task<IHttpActionResult> DeleteUser(string id)
        {
            var appUser = await _applicationUserManager.FindByIdAsync(id);
            if (appUser == null) return NotFound();

            var result = await _applicationUserManager.DeleteAsync(appUser);

            return result.Succeeded ? Ok() : GetErrorResult(result);
        }

        [Authorize(Roles = "Admin")]
        [Route("assign/{id:guid}/roles")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignRolesToUser([FromUri] string id, [FromBody] string[] rolesToAssign)
        {
            var appUser = await _applicationUserManager.FindByIdAsync(id);

            if (appUser == null)
            {
                return NotFound();
            }

            var availableRoles = await _applicationUserManager.GetRolesAsync(appUser.Id);

            var unknownRoles = rolesToAssign.Except(_applicationRoleManager.Roles.Select(x => x.Name)).ToArray();

            if (unknownRoles.Any())
            {

                ModelState.AddModelError("", $"The following Roles do not exist: '{string.Join(",", unknownRoles)}'");
                return BadRequest(ModelState);
            }

            var removeResult = await _applicationUserManager.RemoveFromRolesAsync(appUser.Id, availableRoles.ToArray());

            if (!removeResult.Succeeded)
            {
                ModelState.AddModelError("", $"Failed to remove existing user roles, the followings error occured: {string.Join(",", removeResult.Errors)}");
                return BadRequest(ModelState);
            }

            var addResult = await _applicationUserManager.AddToRolesAsync(appUser.Id, rolesToAssign);

            if (addResult.Succeeded) return Ok();

            ModelState.AddModelError("", $"Failed to add user roles , the followings error occured: {string.Join(", ", addResult.Errors)}");
            return BadRequest(ModelState);
        }

        [Authorize(Roles = "Admin")]
        [Route("claims/{id:guid}/assign")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignClaimsToUser([FromUri] string id, [FromBody] List<ClaimModel> claimsToAssign)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var appUser = await _applicationUserManager.FindByIdAsync(id);

            if (appUser == null)
            {
                return NotFound();
            }

            foreach (var claimModel in claimsToAssign)
            {
                if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                {

                    await _applicationUserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                }

                await _applicationUserManager.AddClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
            }

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [Route("claims/{id:guid}/remove")]
        [HttpPut]
        public async Task<IHttpActionResult> RemoveClaimsFromUser([FromUri] string id, [FromBody] List<ClaimModel> claimsToRemove)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var appUser = await _applicationUserManager.FindByIdAsync(id);

            if (appUser == null)
            {
                return NotFound();
            }

            foreach (var claimModel in claimsToRemove)
            {
                if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                {
                    await _applicationUserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                }
            }

            return Ok();
        }
        
        // POST api/Account/RegisterExternal
        [AllowAnonymous]
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalUserModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var verifiedAccessToken = await VerifyExternalAccessToken(model.Provider, model.ExternalAccessToken);
            if (verifiedAccessToken == null)
            {
                return BadRequest("Invalid Provider or External Access Token");
            }

            var linkedUser = await _applicationUserManager.FindAsync(new UserLoginInfo(model.Provider,verifiedAccessToken.user_id));
            var localUser = await _applicationUserManager.FindByEmailAsync(model.EmailAddress);

            if (linkedUser != null) return BadRequest("External user is already registered");

            if (localUser == null)
            {
                //We need to create a local user for this external login
                localUser = new ApplicationUser { UserName = model.EmailAddress, Email = model.EmailAddress };

                //TODO Figure out what causes this to deadlock.
                var addLocalUser = await _applicationUserManager.CreateAsync(localUser);
                if (!addLocalUser.Succeeded)
                {
                    return GetErrorResult(addLocalUser);
                }
            }          

            //var info = await Authentication.GetExternalLoginInfoAsync();
            var info = new ExternalLoginInfo()
            {
                DefaultUserName = model.EmailAddress,
                Login = new UserLoginInfo(model.Provider, verifiedAccessToken.user_id)
            };

            var addLogin = await _applicationUserManager.AddLoginAsync(localUser.Id, info.Login);
            if (!addLogin.Succeeded)
            {
                return GetErrorResult(addLogin);
            }

            var accessTokenResponse = GenerateLocalAccessTokenResponse(model.EmailAddress);

            return Ok(accessTokenResponse);
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            var ticket = _accessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket?.Identity == null || (ticket.Properties?.ExpiresUtc != null && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            var externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            var result = await _applicationUserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            return result.Succeeded ? Ok() : GetErrorResult(result);
        }
       
        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await _applicationUserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await _applicationUserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            return result.Succeeded ? Ok() : GetErrorResult(result);
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return BadRequest(Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            var redirectUri = string.Empty;
            var redirectUriValidationResult = ValidateClientAndRedirectUri(ref redirectUri);

            if (!string.IsNullOrWhiteSpace(redirectUriValidationResult))
            {
                return BadRequest(redirectUriValidationResult);
            }
            
            var externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            var user = await _applicationUserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            var hasRegistered = user != null;


            //if (hasRegistered)
            //{
            //    Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            //    var oAuthIdentity = await user.GenerateUserIdentityAsync(_applicationUserManager,
            //        OAuthDefaults.AuthenticationType);
            //    var cookieIdentity = await user.GenerateUserIdentityAsync(_applicationUserManager,
            //        CookieAuthenticationDefaults.AuthenticationType);

            //    AuthenticationProperties properties = new AuthenticationProperties(new Dictionary<string, string>
            //    {
            //        { "userName", user.UserName }
            //    });
            //    Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            //}
            //else
            //{
            //    IEnumerable<Claim> claims = externalLogin.GetClaims();
            //    ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
            //    Authentication.SignIn(identity);
            //}

            //return Ok();


            redirectUri = $"{redirectUri}#external_access_token={externalLogin.ExternalAccessToken}&provider={externalLogin.LoginProvider}&haslocalaccount={hasRegistered}&external_user_name={externalLogin.UserName}&external_email_address={externalLogin.EmailAddress}";

            return Redirect(redirectUri);
        }


        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        //[AllowAnonymous]
        //[Route("ExternalLogins")]
        //public IEnumerable<ExternalLoginModel> GetExternalLogins(string returnUrl, bool generateState = false)
        //{
        //    var descriptions = Authentication.GetExternalAuthenticationTypes();

        //    string state;

        //    if (generateState)
        //    {
        //        const int strengthInBits = 256;
        //        state = RandomOAuthStateGenerator.Generate(strengthInBits);
        //    }
        //    else
        //    {
        //        state = null;
        //    }

        //    return descriptions.Select(description => new ExternalLoginModel
        //        {
        //            Name = description.Caption,
        //            Url = Url.Route("ExternalLogin", new
        //            {
        //                provider = description.AuthenticationType,
        //                response_type = "token",
        //                client_id = "self", //TODO?????
        //                redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
        //                state
        //            }),
        //            State = state
        //        })
        //        .ToList();
        //}

        #endregion

        #region Helper Functions 
        /// <summary>
        ///     Validates the client and redirect URI
        /// </summary>
        /// <param name="redirectUriOutput"></param>
        /// <returns></returns>
        private string ValidateClientAndRedirectUri(ref string redirectUriOutput)
        {
            var redirectUriString = GetQueryString(Request, "redirect_uri");

            if (string.IsNullOrWhiteSpace(redirectUriString))
            {
                return "redirect_uri is required";
            }

            Uri redirectUri;
            var validUri = Uri.TryCreate(redirectUriString, UriKind.Absolute, out redirectUri);

            if (!validUri)
            {
                return "redirect_uri is invalid";
            }

            var clientId = GetQueryString(Request, "client_id");

            if (string.IsNullOrWhiteSpace(clientId))
            {
                return "client_Id is required";
            }

            var client = _authRepo.FindClient(clientId);

            if (client == null)
            {
                return $"Client_id '{clientId}' is not registered in the system.";
            }

            if (!string.Equals(client.AllowedOrigin, redirectUri.GetLeftPart(UriPartial.Authority), StringComparison.OrdinalIgnoreCase))
            {
                return $"The given URL is not allowed by Client_id '{clientId}' configuration.";
            }

            redirectUriOutput = redirectUri.AbsoluteUri;

            return string.Empty;
        }

        private static string GetQueryString(HttpRequestMessage request, string key)
        {
            var queryStrings = request.GetQueryNameValuePairs();

            if (queryStrings == null) return null;

            var match = queryStrings.FirstOrDefault(keyValue => string.Compare(keyValue.Key, key, StringComparison.OrdinalIgnoreCase) == 0);

            return string.IsNullOrEmpty(match.Value) ? null : match.Value;
        }

        private async Task<ParsedExternalAccessToken> VerifyExternalAccessToken(string provider, string accessToken)
        {
            ParsedExternalAccessToken parsedToken = null;

            string verifyTokenEndPoint;

            switch (provider)
            {
                case "Facebook":
                    /* You can get it from here: https://developers.facebook.com/tools/accesstoken/
                     * More about debug_tokn here: http://stackoverflow.com/questions/16641083/how-does-one-get-the-app-access-token-for-debug-token-inspection-on-facebook
                     */
                    const string appToken = "1556432234407359|eq-P33fInU7FxAwRqLgz9CfiQiw";
                    verifyTokenEndPoint = $"https://graph.facebook.com/debug_token?input_token={accessToken}&access_token={appToken}";
                    break;
                case "Google":
                    verifyTokenEndPoint = $"https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={accessToken}";
                    break;
                default:
                    return null;
            }

            var client = new HttpClient();
            var uri = new Uri(verifyTokenEndPoint);
            var response = await client.GetAsync(uri);

            if (!response.IsSuccessStatusCode) return null;

            var content = await response.Content.ReadAsStringAsync();

            dynamic jObj = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(content);

            parsedToken = new ParsedExternalAccessToken();

            switch (provider)
            {
                case "Facebook":
                    parsedToken.user_id = jObj["data"]["user_id"];
                    parsedToken.app_id = jObj["data"]["app_id"];

                    if (!string.Equals(_configReader.AuthFacebookAppId, parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }
                    break;
                case "Google":
                    parsedToken.user_id = jObj["user_id"];
                    parsedToken.app_id = jObj["audience"];

                    if (!string.Equals(_configReader.AuthGoogleAppId, parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }
                    break;
            }

            return parsedToken;
        }

        //TODO: https://stackoverflow.com/questions/25739710/how-to-create-refresh-token-with-external-login-provider
        private JObject GenerateLocalAccessTokenResponse(string userName)
        {
            var tokenExpiration = TimeSpan.FromDays(1);

            var identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, userName));
            identity.AddClaim(new Claim("role", "user"));

            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = _accessTokenFormat.Protect(ticket);

            var tokenResponse = new JObject(
                new JProperty("userName", userName),
                new JProperty("access_token", accessToken),
                new JProperty("token_type", "bearer"),
                new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString(CultureInfo.InvariantCulture)),
                new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
                new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString()
                )
            );

            return tokenResponse;
        }

        private UserModel CreateUserModel(ApplicationUser user)
        {
            return new UserModel
            {
                Url = $"/api/Account/User/{user.Id}",
                Id = user.Id,
                UserName = user.UserName,
                //FullName = string.Format("{0} {1}", user.FirstName, user.LastName),
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed,
                //JoinDate = user.JoinDate,
                Roles = _applicationUserManager.GetRolesAsync(user.Id).Result,
                Claims = _applicationUserManager.GetClaimsAsync(user.Id).Result
            };
        }

        #endregion

        #region Private Classes

        private class ExternalLoginData
        {
            public string LoginProvider { get; private set; }
            public string ProviderKey { get; private set; }
            public string UserName { get; private set; }
            public string EmailAddress { get; private set; }
            public string ExternalAccessToken { get; private set; }

            public IEnumerable<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider)
                };

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                if (!string.IsNullOrWhiteSpace(EmailAddress))
                {
                    claims.Add(new Claim(ClaimTypes.Email, EmailAddress, null, LoginProvider));
                }
                
                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                var providerKeyClaim = identity?.FindFirst(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(providerKeyClaim?.Issuer) || string.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name),
                    EmailAddress = identity.FindFirstValue(ClaimTypes.Email),
                    ExternalAccessToken = identity.FindFirstValue("ExternalAccessToken"),
                };
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _applicationUserManager.Dispose();
                _authRepo.Dispose();
            }

            base.Dispose(disposing);
        }

        #endregion

    }
}
