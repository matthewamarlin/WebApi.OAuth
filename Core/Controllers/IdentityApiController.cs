﻿using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace SpearOne.Examples.OAuth.Service.Core.Controllers
{
    public class IdentityApiController : ApiController
    {
        public IdentityApiController() { }

        public IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (result.Succeeded) return null;
            if (result.Errors != null)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            if (ModelState.IsValid)
            {
                // No ModelState errors are available to send, so just return an empty BadRequest.
                return BadRequest();
            }

            return BadRequest(ModelState);
        }
    }
}
