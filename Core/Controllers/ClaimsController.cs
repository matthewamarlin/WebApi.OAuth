﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace SpearOne.Examples.OAuth.Service.Core.Controllers
{
    [RoutePrefix("api/claims")]
    public class ClaimsController : ApiController
    {
        [Authorize]
        [Route("")]
        public IHttpActionResult GetClaims()
        {
            var identity = User.Identity as ClaimsIdentity;

            //Technically this shouldn't happen if we [Authorize] the user.
            if (identity == null) return InternalServerError(new Exception("The given users identity could not be determined"));

            var claims = from c in identity.Claims
                         select new
                         {
                             subject = c.Subject.Name,
                             type = c.Type,
                             value = c.Value
                         };

            return Ok(claims);
        }

    }
}