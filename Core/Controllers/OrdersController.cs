﻿using System.Web.Http;
using SpearOne.Examples.OAuth.Service.Core.Identity;

namespace SpearOne.Examples.OAuth.Service.Core.Controllers
{
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        [Authorize(Roles = "IncidentResolvers")]
        [HttpPut]
        [Route("refund/{orderId}")]
        public IHttpActionResult RefundOrder([FromUri]string orderId)
        {
            return Ok();
        }

        [ClaimsAuthorization(ClaimType = "FTE", ClaimValue = "1")]
        [Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok();
        }
    }
}
