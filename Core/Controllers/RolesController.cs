﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SpearOne.Examples.OAuth.Service.Core.Identity;
using SpearOne.Examples.OAuth.Service.Core.Models;

namespace SpearOne.Examples.OAuth.Service.Core.Controllers
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/roles")]
    public class RolesController : IdentityApiController
    {
        private readonly ApplicationRoleManager _applicationRoleManager;
        private readonly ApplicationUserManager _applicationUserManager;

        public RolesController(ApplicationRoleManager applicationRoleManager, ApplicationUserManager applicationUserManager)
        {
            _applicationRoleManager = applicationRoleManager;
            _applicationUserManager = applicationUserManager;
        }

        [Route("{id:guid}", Name = "GetRoleById")]
        public async Task<IHttpActionResult> GetRole(string id)
        {
            var role = await _applicationRoleManager.FindByIdAsync(id);

            if (role != null)
            {
                return Ok(CreateRoleModel(role));
            }

            return NotFound();
        }

        [Route("", Name = "GetAllRoles")]
        public IHttpActionResult GetAllRoles()
        {
            var roles = _applicationRoleManager.Roles;

            return Ok(roles);
        }

        [Route("create")]
        public async Task<IHttpActionResult> Create(CreateRoleModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var role = new IdentityRole { Name = model.Name };

            var result = await _applicationRoleManager.CreateAsync(role);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            Uri locationHeader = new Uri(Url.Link("GetRoleById", new { id = role.Id }));

            return Created(locationHeader, CreateRoleModel(role));

        }

        [Route("{id:guid}")]
        public async Task<IHttpActionResult> DeleteRole(string id)
        {
            var role = await _applicationRoleManager.FindByIdAsync(id);

            if (role == null) return NotFound();

            var result = await _applicationRoleManager.DeleteAsync(role);

            return result.Succeeded ? Ok() : GetErrorResult(result);
        }

        [Route("ManageUsersInRole")]
        public async Task<IHttpActionResult> ManageUsersInRole(UsersInRoleModel model)
        {
            var role = await _applicationRoleManager.FindByIdAsync(model.Id);

            if (role == null)
            {
                ModelState.AddModelError("", "Role does not exist");
                return BadRequest(ModelState);
            }

            foreach (var user in model.EnrolledUsers)
            {
                var appUser = await _applicationUserManager.FindByIdAsync(user);

                if (appUser == null)
                {
                    ModelState.AddModelError("", $"User: {user} does not exists");
                    continue;
                }

                if (_applicationUserManager.IsInRole(user, role.Name)) continue;

                var result = await _applicationUserManager.AddToRoleAsync(user, role.Name);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", $"User: {user} could not be added to role");
                }
            }

            foreach (var user in model.RemovedUsers)
            {
                var appUser = await _applicationUserManager.FindByIdAsync(user);

                if (appUser == null)
                {
                    ModelState.AddModelError("", $"User: {user} does not exists");
                    continue;
                }

                var result = await _applicationUserManager.RemoveFromRoleAsync(user, role.Name);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", $"User: {user} could not be removed from role");
                }
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        public RoleModel CreateRoleModel(IdentityRole appRole)
        {
            return new RoleModel
            {
                Url = $"api/Roles/{appRole.Id}", //TODO Use UrlHelper
                Id = appRole.Id,
                Name = appRole.Name
            };
        }
    }
}
