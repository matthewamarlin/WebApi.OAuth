﻿using System;
using System.Configuration;

namespace SpearOne.Examples.OAuth.Service.Core
{
    //TODO Validate Configuration before reading??
    public class ConfigReader
    {
        public int AuthAccessTokenExpireTimeMinutes = int.Parse(ConfigurationManager.AppSettings["Auth.AccessTokenExpireTimeMinutes"]); 

        public string AuthAudienceId => ConfigurationManager.AppSettings["Auth.AudienceId"];
        public string AuthAudienceSecret => ConfigurationManager.AppSettings["Auth.AudienceSecret"];

        public string AuthFacebookAppId => ConfigurationManager.AppSettings["Auth.Facebook.AppId"];
        public string AuthFacebookSecret => ConfigurationManager.AppSettings["Auth.Facebook.AppSecret"];
        public string AuthGoogleAppId => ConfigurationManager.AppSettings["Auth.Google.AppId"];
        public string AuthGoogleSecret => ConfigurationManager.AppSettings["Auth.Google.Secret"];

        public string SendGridUser => ConfigurationManager.AppSettings["SendGrid.User"];
        public string SendGridPassword => ConfigurationManager.AppSettings["SendGrid.Password"];
        public string SendGridFromAddress => ConfigurationManager.AppSettings["SendGrid.FromAddress"];
        public string SendGridFromDisplayName => ConfigurationManager.AppSettings["SendGrid.FromDisplayName"];
    }
}
