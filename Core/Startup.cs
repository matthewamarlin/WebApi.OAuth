﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Owin;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using SpearOne.Examples.OAuth.Service.Core;
using SpearOne.Examples.OAuth.Service.Core.Identity;
using SpearOne.Examples.OAuth.Service.Core.Identity.Providers;
using SpearOne.Examples.OAuth.Service.Core.Migrations;
using SpearOne.Examples.OAuth.Service.Core.Services;

[assembly: OwinStartup(typeof(Startup))]
namespace SpearOne.Examples.OAuth.Service.Core
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var container = ConfigureSimpleInjector(app);
            
            /* Security Configurations */
            app.UseCookieAuthentication(new CookieAuthenticationOptions()); //TODO Whats this going to do???
            ConfigureOAuth(app, container);
            
            app.UseCors(CorsOptions.AllowAll);
            
            ConfigureWebApi(app, container);
            ConfigureSignalR(app, container);

            /* Database Initialization */
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AuthDbContext, AuthDbMigrationsConfiguration>());
            var db = new AuthDbContext();
            db.Database.Initialize(true);   // force database creation
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void ConfigureSignalR(IAppBuilder app, Container container)
        {
            app.Map("/signalr", builder =>
            {
                builder.MapSignalR();
            });
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void ConfigureWebApi(IAppBuilder app, Container container)
        {
            var config = new HttpConfiguration
            {
                DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container)
            };

            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            //Attribute Based Routing with Fallback Routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional }
            );

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            config.EnsureInitialized();
            app.UseWebApi(config);
        }

        private Container ConfigureSimpleInjector(IAppBuilder app)
        {
            var container = new Container();

            /* 
             * Note: For Web API applications the use of the AsyncScopedLifestyle is advised over the WebRequestLifestyle. 
             * Web API Request Object Lifestyle Management: http://simpleinjector.readthedocs.io/en/latest/lifetimes.html#asyncscoped-vs-webrequest
             */
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            //Create Logging Framework
            container.RegisterConditional(typeof(ILogger),
                x => typeof(Log4NetWrapper<>).MakeGenericType(x.Consumer.ImplementationType),
                Lifestyle.Singleton,
                x => true);

            // IoC for ASP.NET Identity
            container.RegisterSingleton(app);
            container.Register<ApplicationUserManager>(Lifestyle.Scoped);
            container.Register<ApplicationRoleManager>(Lifestyle.Scoped);
            container.Register<AuthRepository>(Lifestyle.Scoped);
            container.Register<AuthDbContext>(Lifestyle.Scoped);
            container.Register<IRoleStore<IdentityRole, string>>( () => new RoleStore<IdentityRole>(container.GetInstance<AuthDbContext>()), Lifestyle.Scoped);
            container.Register<IUserStore<ApplicationUser>>( () => new UserStore<ApplicationUser>(container.GetInstance<AuthDbContext>()), Lifestyle.Scoped);
            container.Register<IEmailService, EmailService>(Lifestyle.Scoped);
            container.Register<ConfigReader>(Lifestyle.Singleton);
            container.Register<ISecureDataFormat<AuthenticationTicket>, SecureDataFormat<AuthenticationTicket>>(Lifestyle.Scoped);
            container.Register<ITextEncoder, Base64UrlTextEncoder>(Lifestyle.Scoped);
            container.Register<IDataSerializer<AuthenticationTicket>, TicketSerializer>(Lifestyle.Scoped);
            container.Register(() => new DpapiDataProtectionProvider().Create("ASP.NET Identity"), Lifestyle.Scoped);

            // https://ilovecsharp.com/2016/10/08/porting-your-webapi-2-2-app-to-azure-service-fabric/
            // https://stackoverflow.com/questions/28606676/how-can-i-instantiate-owin-idataprotectionprovider-in-azure-web-jobs
            container.Register<IDataProtectionProvider, MachineKeyProtectionProvider>(Lifestyle.Singleton);

            // This is an extension method from the integration package.
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration, Assembly.GetExecutingAssembly());

            //Finalization
            container.Verify();

            return container;
        }
        
        public void ConfigureOAuth(IAppBuilder app, Container container)
        {
            var configReader = container.GetInstance<ConfigReader>();

            var issuer = "http://localhost:4200"; //TODO Configurable

            /* Use a cookie to temporarily store information about a user logging in with a third party login provider */
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            var oAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true, //TODO In production mode set AllowInsecureHttp = false
                TokenEndpointPath = new PathString("/oauth/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(configReader.AuthAccessTokenExpireTimeMinutes), //TODO Config Items
                AccessTokenFormat = new CustomJwtFormat(container.GetInstance<ConfigReader>(), issuer), //TODO Config
                Provider = new AuthorizationServerProvider(container),
                RefreshTokenProvider = new RefreshTokenProvider() 
            };

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);

            /* Token Generation */
            //app.UseOAuthBearerTokens(oAuthServerOptions); //This is a helper that basically calls these 2 methods 
            //app.UseOAuthAuthorizationServer(OAuthServerOptions);
            //app.UseOAuthBearerAuthentication(OAuthServerOptions);

            /* Configure Google External Login 
             * https://developers.google.com/identity/protocols/OAuth2
             */
            var googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = configReader.AuthGoogleAppId,
                ClientSecret = configReader.AuthGoogleSecret,
                Provider = new GoogleAuthProvider(),
            };
            app.UseGoogleAuthentication(googleAuthOptions);


            /* Configure Facebook External Login
             * https://developers.facebook.com/docs/facebook-login/access-tokens
             * https://developers.facebook.com/apps/1556432234407359/dashboard/
             */
            var facebookAuthOptions = new FacebookAuthenticationOptions()
            {
                AppId = configReader.AuthFacebookAppId, 
                AppSecret = configReader.AuthFacebookSecret,
                
                /* Issues with getting user email:
                 * https://stackoverflow.com/a/35367347/985856
                 * https://stackoverflow.com/a/32636149/985856
                 */
                UserInformationEndpoint = "https://graph.facebook.com/v2.4/me?fields=id,name,email,first_name,last_name",
                Scope = { "email" },
                BackchannelHttpHandler = new FacebookBackChannelHandler(),
                Provider = new FacebookAuthProvider()
            };
            app.UseFacebookAuthentication(facebookAuthOptions);


            //Token Consumption
            var audienceId = configReader.AuthAudienceId;
            var audienceSecret = TextEncodings.Base64Url.Decode(configReader.AuthAudienceSecret);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { audienceId },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret)
                    }
                });
        }
    }

}