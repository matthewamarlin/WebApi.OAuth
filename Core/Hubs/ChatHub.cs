﻿using SpearOne.Common.Logging;
using Microsoft.AspNet.SignalR;
using SpearOne.Examples.OAuth.Service.Contracts;

namespace SpearOne.Examples.OAuth.Service.Core.Hubs
{
    public class ChatHub : Hub<IChatHubClient>, IChatHub
    {
        private readonly ILogger _log;

        public ChatHub(ILogger log)
        {
            _log = log;
        }

        public void Send(string sender, string message)
        {
            _log.Write(Level.Debug, $"{nameof(Send)}: {message}");
            Clients.Others.ReceiveMessage(sender, message);
        }
    }
}