﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SpearOne.Examples.OAuth.Service.Core.Entities
{
    /// <summary>
    /// Represents an oAuth RefreshToken, 
    /// See https://auth0.com/learn/refresh-tokens/
    /// </summary>
    public class RefreshToken
    {
        [Key]
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Subject { get; set; }

        [Required]
        [MaxLength(50)]
        public string ClientId { get; set; }

        [Required]
        public string ProtectedTicket { get; set; }

        // Used for display purposes only
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiresUtc { get; set; }
    }
}