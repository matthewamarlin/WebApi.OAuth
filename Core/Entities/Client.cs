﻿using System.ComponentModel.DataAnnotations;
using SpearOne.Examples.OAuth.Service.Core.Models;

namespace SpearOne.Examples.OAuth.Service.Core.Entities
{
    /// <summary>
    ///     Represents a client / application which will be used for authentication. 
    /// </summary>
    public class Client
    {
        [Key]
        public string Id { get; set; }

        [Required]
        public string Secret { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public ApplicationTypes ApplicationType { get; set; }

        public bool Active { get; set; }

        public int RefreshTokenLifeTime { get; set; }

        [MaxLength(100)]
        public string AllowedOrigin { get; set; }
    }
}