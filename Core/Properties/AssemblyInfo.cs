﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.OAuth.Service.Core")]
[assembly: AssemblyDescription("The core library for the SpearOne.Examples.OAuth.Service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("BCED8600-78FC-45C5-94F8-742260E4AA15")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]