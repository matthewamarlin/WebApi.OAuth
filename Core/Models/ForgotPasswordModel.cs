﻿using System.ComponentModel.DataAnnotations;

namespace SpearOne.Examples.OAuth.Service.Core.Models
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        public string RedirectUri { get; set; }
    }
}
