﻿namespace SpearOne.Examples.OAuth.Service.Core.Models
{
    public enum ApplicationTypes
    {
        JavaScript = 0,
        NativeConfidential = 1
    };    
}