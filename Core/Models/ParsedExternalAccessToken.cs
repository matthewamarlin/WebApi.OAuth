namespace SpearOne.Examples.OAuth.Service.Core.Models
{
    public class ParsedExternalAccessToken
    {
        // ReSharper disable once InconsistentNaming
        public string user_id { get; set; }
        // ReSharper disable once InconsistentNaming
        public string app_id { get; set; }
    }
}