﻿namespace SpearOne.Examples.OAuth.Service.Core.Models
{
    public class RoleModel
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
