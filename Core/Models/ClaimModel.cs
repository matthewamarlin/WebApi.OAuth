﻿using System.ComponentModel.DataAnnotations;

namespace SpearOne.Examples.OAuth.Service.Core.Models
{
    public class ClaimModel
    {
        [Required]
        [Display(Name = "Claim Type")]
        public string Type { get; set; }

        [Required]
        [Display(Name = "Claim Value")]
        public string Value { get; set; }
    }
}