﻿using System.ComponentModel.DataAnnotations;

namespace SpearOne.Examples.OAuth.Service.Core.Models
{
    public class AddExternalLoginModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }
}
