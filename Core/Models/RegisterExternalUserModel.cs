using System.ComponentModel.DataAnnotations;

namespace SpearOne.Examples.OAuth.Service.Core.Models
{
    public class RegisterExternalUserModel
    {
        [Required]
        public string EmailAddress { get; set; }

        [Required]
        public string Provider { get; set; }

        [Required]
        public string ExternalAccessToken { get; set; }

    }
}