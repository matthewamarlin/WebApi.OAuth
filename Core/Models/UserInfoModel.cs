﻿using System;

namespace SpearOne.Examples.OAuth.Service.Core.Models
{
    public class UserInfoModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime JoinDate { get; set; }
        public bool HasLocalAccount { get; set; }
        public string LoginProvider { get; set; }
    }
}
