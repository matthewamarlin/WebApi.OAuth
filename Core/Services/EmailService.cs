﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace SpearOne.Examples.OAuth.Service.Core.Services
{
    public class EmailService : IEmailService
    {
        private readonly ConfigReader _configReader;

        public EmailService(ConfigReader configReader)
        {
            _configReader = configReader;
        }

        public async Task SendAsync(IdentityMessage message)
        {
            // SendMail(message);
            await SendMailAsync(message);
        }

        public async Task SendMailAsync(IdentityMessage message)
        {
            var mailMessage = new MailMessage
            {
                Body = message.Body,
                Subject = message.Subject,
            };

            mailMessage.To.Add(message.Destination);

            var smtpClient = new SmtpClient();
            smtpClient.SendCompleted += (sender, eventArgs) => {
                smtpClient.Dispose();
                mailMessage.Dispose();
            };

            await smtpClient.SendMailAsync(mailMessage);
        }

        public void SendMail(IdentityMessage message)
        {
            var mailMessage = new MailMessage
            {
                Body = message.Body,
                Subject = message.Subject,
            };

            mailMessage.To.Add(message.Destination);

            var smtpClient = new SmtpClient();
            smtpClient.SendCompleted += (sender, eventArgs) => {
                smtpClient.Dispose();
                mailMessage.Dispose();
            };

            smtpClient.Send(mailMessage);
        }
    }
}