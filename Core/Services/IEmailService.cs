﻿using Microsoft.AspNet.Identity;

namespace SpearOne.Examples.OAuth.Service.Core.Services
{
    public interface IEmailService : IIdentityMessageService
    { }
}