﻿using System.IO;

namespace SpearOne.Examples.OAuth.Service.Core.Common
{
    public static class Encoding
    {
        public static string BytesToString(this byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }
    }
}
