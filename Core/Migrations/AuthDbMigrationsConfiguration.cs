using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SpearOne.Examples.OAuth.Service.Core.Entities;
using SpearOne.Examples.OAuth.Service.Core.Extensions;
using SpearOne.Examples.OAuth.Service.Core.Identity;

namespace SpearOne.Examples.OAuth.Service.Core.Migrations
{
    internal sealed class AuthDbMigrationsConfiguration : DbMigrationsConfiguration<AuthDbContext>
    {
        public AuthDbMigrationsConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AuthDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //Configure Default Clients
            if (context.Clients.Any()) { return; }
            context.Clients.AddRange(BuildClientsList());

            //Configure Default Users
            ConfigureDefaultRoles();
            ConfigureDefaultUsers();

            context.SaveChanges();
        }

        #region Actions

        private static void ConfigureDefaultRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new AuthDbContext()));

            if (roleManager.Roles.Any()) return;

            roleManager.Create(new IdentityRole { Name = "SystemAdmin" });
            roleManager.Create(new IdentityRole { Name = "Admin" });
            roleManager.Create(new IdentityRole { Name = "User" });
        }

        private static void ConfigureDefaultUsers()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new AuthDbContext()));

            //TODO Change to real production admin / make configurable
            var user = new ApplicationUser()
            {
                UserName = "admin",
                Email = "admin@example.com", 
                EmailConfirmed = true,
                FirstName = "Matthew",
                LastName = "Marlin",
                JoinDate = DateTime.Now.AddYears(-3)
            };

            //TODO Make configurable
            userManager.Create(user, "Adm1n!"); 

            var adminUser = userManager.FindByName(user.UserName);

            userManager.AddToRoles(adminUser.Id, "SystemAdmin", "Admin");
        }

        private static IEnumerable<Client> BuildClientsList()
        {
            return new List<Client>
            {
                new Client
                {
                    Id = "SpearOne.Examples.OAuth.Website.Local",
                    Secret = HashExtension.GetHash("abc@123"),
                    Name = "Primary Angular front-end Application",
                    ApplicationType =  Models.ApplicationTypes.JavaScript,
                    Active = true,
                    RefreshTokenLifeTime = 7200,
                    AllowedOrigin = "http://localhost:4200"
                },
                new Client
                {
                    Id = "SpearOne.Examples.OAuth.Website.External",
                    Secret = HashExtension.GetHash("abc@123"),
                    Name = "Primary Angular front-end Application",
                    ApplicationType =  Models.ApplicationTypes.JavaScript,
                    Active = true,
                    RefreshTokenLifeTime = 7200,
                    AllowedOrigin = "http://angular-oauth.examples.spearone.net"
                }
            };
        }

        #endregion
    }
}
