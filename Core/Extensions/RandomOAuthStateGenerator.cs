﻿using System;
using System.Security.Cryptography;
using System.Web;

namespace SpearOne.Examples.OAuth.Service.Core.Extensions
{
    public static class RandomOAuthStateGenerator
    {
        private static readonly RandomNumberGenerator Random = new RNGCryptoServiceProvider();

        public static string Generate(int strengthInBits)
        {
            const int bitsPerByte = 8;

            if (strengthInBits % bitsPerByte != 0)
            {
                throw new ArgumentException(@"strengthInBits must be evenly divisible by 8.", nameof(strengthInBits));
            }

            var strengthInBytes = strengthInBits / bitsPerByte;

            var data = new byte[strengthInBytes];
            Random.GetBytes(data);
            return HttpServerUtility.UrlTokenEncode(data);
        }
    }
}
