using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Examples.OAuth.Service.Tests.Unit")]
[assembly: AssemblyDescription("This assembly contains all the Unit test logic for the SpearOne.Examples.OAuth.Service")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("0E6E2EFD-6D9A-4FE3-8E95-39DA8318B720")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]